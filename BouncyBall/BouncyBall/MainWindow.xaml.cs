﻿namespace BouncyBall
{
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            this.VisualTextRenderingMode = TextRenderingMode.Aliased;

            RenderOptions.SetEdgeMode(this.mainWindow, EdgeMode.Aliased);
            RenderOptions.SetBitmapScalingMode(this.mainWindow, BitmapScalingMode.HighQuality);
        }
    }
}
