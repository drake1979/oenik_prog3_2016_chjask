﻿namespace BouncyBall.Controller
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.Text;
    using System.Windows;
    using System.Windows.Media;

    using Helpers;
    using Model;

    using Brushes = System.Windows.Media.Brushes;
    using Pen = System.Windows.Media.Pen;
    using Point = System.Windows.Point;

    /// <summary>
    /// Partial for GameController class responsible for game content rendering only
    /// </summary>
    public partial class GameController
    {
        /// <summary>
        /// Render content when game is paused
        /// </summary>
        /// <param name="drawingContext">
        /// The drawing context.
        /// </param>
        private void DoRenderPausedContent(DrawingContext drawingContext)
        {
            string score = $"Your score is : {_score}";
            string info = "Press space to start the game.";
            string toDisplay = $"{(_score > 0 ? score : String.Empty)}\r\n{info}";
            FormattedText fText = new FormattedText(
                toDisplay,
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Arial"),
                50.0, 
                Brushes.DarkOrange,
                null,
                1);
            
            drawingContext.DrawText(fText, new Point(20, 20));
        }

        /// <summary>
        /// Render game content when in game
        /// </summary>
        /// <param name="drawingContext">
        ///     The drawing context.
        /// </param>
        private void DoRenderGameContent(DrawingContext drawingContext)
        {
            if (_ball != null)
            {
                ImageSource source = DrawingTools.BirdSource();
                drawingContext.DrawImage(source, _ball.Rectangle);
            }

            if (_obstacles != null)
            {
                foreach (Obstacle obstacle in _obstacles)
                {
                    drawingContext.DrawRoundedRectangle(
                    DrawingTools.ObstacleBrush(),
                    new Pen(DrawingTools.BlackPieceBorder, 1),
                    obstacle.Rectangle,
                    10,
                    10);
                }
            }

            if (_score >= 0)
            {
                string scoreString = $"Score: {_score}";
                
                FormattedText fText = new FormattedText(
                    scoreString,
                    CultureInfo.CurrentCulture,
                    FlowDirection.LeftToRight,
                    new Typeface("Arial"),
                    50.0,
                    Brushes.SaddleBrown,
                    null,
                    1);

                drawingContext.DrawText(fText, new Point(20, 20));
            }

            StringBuilder infoSb = new StringBuilder();
            infoSb.AppendLine($"Gravity: {Math.Round(_acc.Y/this._verticalAccNormalizer, 2)} [m/s^2]");

            Debug.Assert(_ball != null, "_ball != null");
            infoSb.AppendLine($"SuperBounce: {_ball.SuperBounce}");
            infoSb.AppendLine($"SmushyBounce: {_ball.SmushyBounce}");
            infoSb.AppendLine($"ObstacleSpeed: {Math.Round(_obstacleBaseSpeed/this._verticalAccNormalizer, 2)} [m/s]");
            infoSb.AppendLine($"BallSpeed X: {Math.Round(_ball.Speed.X/this._verticalAccNormalizer, 2)} [m/s]");
            infoSb.AppendLine($"BallSpeed Y: {Math.Round(_ball.Speed.Y/this._verticalAccNormalizer, 2)} [m/s]");
            infoSb.AppendLine($"FPS: { Math.Round(1000 / _dT.TotalMilliseconds, MidpointRounding.ToEven) } [fps]");

            FormattedText infoText = new FormattedText(
                infoSb.ToString(),
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Arial"),
                10.0, 
                Brushes.Black,
                null,
                1);
            
            drawingContext.DrawText(infoText, new Point(ActualWidth - 150, 20));
        }
    }
}
