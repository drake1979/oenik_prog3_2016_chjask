﻿namespace BouncyBall.Controller
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;

    using Model;

    /// <summary>
    /// Main class managing the game and it's movements.
    /// </summary>
    public partial class GameController : FrameworkElement
    {
        #region private read-only fields

        /// <summary>
        /// The absolute value for horizontal speed change unit.
        /// </summary>
        private const int DVxAbs = 100;

        /// <summary>
        /// The absolute value for vertival speed change unit
        /// </summary>
        private const int DVyAbs = 150;

        /// <summary>
        /// The ball radius.
        /// </summary>
        private const int BallRadius = 50;

        /// <summary>
        /// The vertical acceleration normalization target.
        /// </summary>
        private const double DefaultVerticalAccNormalizeTarget = 9.81;

        /// <summary>
        /// Default obstacle speed
        /// </summary>
        private const double DefaultObstacleBaseSpeed = -30;

        /// <summary>
        /// The default obstacle speed variation.
        /// </summary>
        private const double DefaultObstacleSpeedVariation = 30;

        /// <summary>
        /// The default base obstacle interval seconds.
        /// </summary>
        private const double DefaultBaseObstacleIntervalSeconds = 10;

        /// <summary>
        /// The initial vertical acceleration.
        /// </summary>
        private const int DefauldVerticalAcceleration = 700;

        /// <summary>
        /// Acceleration normalizer ratio. Used to scale the default px/s^2 acceleration to m/s^2
        /// </summary>
        private readonly double _verticalAccNormalizer;

        /// <summary>
        /// Timer for game movement and graphics iteration.
        /// </summary>
        private readonly DispatcherTimer _timer = new DispatcherTimer();

        /// <summary>
        /// Timer for movement control key's command repetition.
        /// </summary>
        private readonly DispatcherTimer _commandRepeatTimer = new DispatcherTimer();

        /// <summary>
        /// Stopwatch to measure time elapsed between iterations.
        /// </summary>
        private readonly Stopwatch _stopWatch = new Stopwatch();

        /// <summary>
        /// Used randomize obstacle generation.
        /// </summary>
        private readonly Random _rand = new Random(DateTime.Now.GetHashCode());

        /// <summary>
        /// The _media player.
        /// </summary>
        private readonly MediaPlayer _musicPlayer;

        /// <summary>
        /// The _media player.
        /// </summary>
        private readonly MediaPlayer _bouncePlayer;

        #endregion private read-only fields

        #region private fields

        /// <summary>
        /// Field representimg the Ball object.
        /// </summary>
        private Ball _ball;

        /// <summary>
        /// Field for list of generated obstacles.
        /// </summary>
        private List<Obstacle> _obstacles;

        /// <summary>
        /// The current vertical acceleration value. (gravity)
        /// </summary>
        private Vector _acc;

        /// <summary>
        /// Scaling ration for vertical speed change unit dependent upon current vertical acceleration.
        /// It's purposi is to keep vertical speed control in a well controllable interval.
        /// </summary>
        private double _currentToOriginalSqrtAccRatio;

        /// <summary>
        /// Boolean field indicating that the up control button is pressed.
        /// </summary>
        private bool _upPressed;

        /// <summary>
        /// Boolean field indicating thet the down control button is pressed.
        /// </summary>
        private bool _downPressed;

        /// <summary>
        /// Boolean field indicating that the right control button is pressed
        /// </summary>
        private bool _rightPressed;

        /// <summary>
        /// Boolean field indicating that the left control button is pressed
        /// </summary>
        private bool _leftPressed;

        /// <summary>
        /// Boolean field indicating that the spave control button is pressed
        /// </summary>
        private bool _spacePressed;

        /// <summary>
        /// Storint the time when the previous obstacle has been generated used for randomized obstacle generation
        /// </summary>
        private DateTime _prevObstacle = DateTime.Now;

        /// <summary>
        /// Current horizontal obstacle speed
        /// </summary>
        private double _obstacleBaseSpeed = -30;

        /// <summary>
        /// Current obstacle speed variation (horizontal speed randomization base)
        /// </summary>
        private double _obstacleSpeedVariation = 30;

        /// <summary>
        /// The current score
        /// </summary>
        private int _score;

        /// <summary>
        /// The default obstacle generation interval in seconds
        /// </summary>
        private double _baseObstacleIntervalSeconds = 10;

        /// <summary>
        /// Private field containing the total time from game start
        /// </summary>
        private TimeSpan _sumT;

        /// <summary>
        /// Provate field containing the time elapsed from previous iteration
        /// </summary>
        private TimeSpan _dT;

        #endregion private fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GameController"/> class.
        /// </summary>
        public GameController()
        {
            _acc = new Vector(0, DefauldVerticalAcceleration);
            _verticalAccNormalizer = _acc.Y / DefaultVerticalAccNormalizeTarget;
            _currentToOriginalSqrtAccRatio = 1;

            var path = Path.Combine(Environment.CurrentDirectory, "Resources", "angryMusic.mp3");
            var uri = new Uri(path);
            _musicPlayer = new MediaPlayer();
            _musicPlayer.Open(uri);
            _musicPlayer.MediaEnded += MusicPlayerOnMusicEnded;

            path = Path.Combine(Environment.CurrentDirectory, "Resources", "bounce.wav");
            uri = new Uri(path);
            _bouncePlayer = new MediaPlayer();
            _bouncePlayer.Open(uri);

            Loaded += GameControllerLoaded;
        }

        #endregion Constructors

        #region Event Handlers

        /// <summary>
        /// The on render.
        /// </summary>
        /// <param name="drawingContext">
        /// The drawing context.
        /// </param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (_timer.IsEnabled)
            {
                DoRenderGameContent(drawingContext);
            }
            else
            {
                DoRenderPausedContent(drawingContext);
            }
        }

        /// <summary>
        /// The media player on media ended.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="eventArgs">
        /// The event args.
        /// </param>
        private void MusicPlayerOnMusicEnded(object sender, EventArgs eventArgs)
        {
            if (_timer.IsEnabled)
            {
                _musicPlayer.Position = TimeSpan.Zero;
                _musicPlayer.Play();
            }
        }

        /// <summary>
        /// Initialize game.
        /// </summary>
        /// <param name="sender">
        /// The sender default parameter
        /// </param>
        /// <param name="e">
        /// The eventArgs default parameter
        /// </param>
        private void GameControllerLoaded(object sender, RoutedEventArgs e)
        {
            Focusable = true;
            Focus();

            UseLayoutRounding = false;
            VisualEdgeMode = EdgeMode.Unspecified;
            VisualBitmapScalingMode = BitmapScalingMode.Linear;

            KeyDown += GameControllerKeyDown;
            KeyUp += GameControllerKeyUp;

            _timer.Interval = new TimeSpan(0, 0, 0, 0, 5);
            _timer.Tick += TimerTick;

            _commandRepeatTimer.Interval = new TimeSpan(0, 0, 0, 0, 35);
            _commandRepeatTimer.Tick += CommandRepeatTimerTick;
            
            InvalidateVisual();
        }

        /// <summary>
        /// The timer tick.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TimerTick(object sender, EventArgs e)
        {
            _dT = _stopWatch.Elapsed - _sumT;
            _sumT = _stopWatch.Elapsed;

            DoIterateGame(_dT);

            if (_ball.IntersectsWith(_obstacles.Select(o => o.Geometry)))
            {
                GameOver();
            }

            InvalidateVisual();
        }

        /// <summary>
        /// Process _commandTimer tick event.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void CommandRepeatTimerTick(object sender, EventArgs e)
        {
            if (_rightPressed)
            {
                _ball.Speed = new Vector(_ball.Speed.X + DVxAbs, _ball.Speed.Y);
            }

            if (_leftPressed)
            {
                _ball.Speed = new Vector(_ball.Speed.X - DVxAbs, _ball.Speed.Y);
            }

            if (_spacePressed)
            {
                _ball.Speed = new Vector(_ball.Speed.X, _ball.Speed.Y - DVyAbs * _currentToOriginalSqrtAccRatio);
            }
        }

        /// <summary>
        /// Process key up event
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void GameControllerKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                _rightPressed = false;
            }

            if (e.Key == Key.Left)
            {
                _leftPressed = false;
            }

            if (e.Key == Key.Space)
            {
                _spacePressed = false;
            }
        }

        /// <summary>
        /// Process key down event
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void GameControllerKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                _rightPressed = true;
                _leftPressed = false;
            }

            if (e.Key == Key.Left)
            {
                _leftPressed = true;
                _rightPressed = false;
            }

            if (e.Key == Key.Space)
            {
                _spacePressed = true;
            }

            if (_timer.IsEnabled)
            {
                if (e.Key == Key.Up)
                {
                    _upPressed = true;
                    _downPressed = false;
                }
                else if (e.Key == Key.Down)
                {
                    _downPressed = true;
                    _upPressed = false;
                }
                else if (e.Key == Key.PageUp)
                {
                    double multiplyer = Math.Sqrt(2);
                    ScaleObstaclesSpeed(multiplyer);
                }
                else if (e.Key == Key.PageDown)
                {
                    double multiplyer = 1 / Math.Sqrt(2);
                    ScaleObstaclesSpeed(multiplyer);
                }
                else if (e.Key == Key.Add)
                {
                    double multiplyer = Math.Sqrt(2);
                    ScaleAcceleration(multiplyer);
                }
                else if (e.Key == Key.Subtract)
                {
                    double multiplyer = 1 / Math.Sqrt(2);
                    ScaleAcceleration(multiplyer);
                }
            }
            else
            {
                if (e.Key == Key.Space)
                {
                    StartGame();
                }
            }

            InvalidateVisual();
        }
        
        #endregion Event Handlers

        #region Operation

        /// <summary>
        /// Scale acceleration with multiplier and scale other variables for better game experience
        /// </summary>
        /// <param name="multiplyer">
        /// The multiplyer.
        /// </param>
        private void ScaleAcceleration(double multiplyer)
        {
            _acc = Vector.Multiply(multiplyer, _acc);
            _ball.Acceleration = _acc;
            _currentToOriginalSqrtAccRatio = Math.Abs(_acc.Y) / DefauldVerticalAcceleration;
        }

        /// <summary>
        /// Scale obstacle speed and other obstacle generation variables with multiplyer
        /// </summary>
        /// <param name="multiplyer">
        /// The multiplyer.
        /// </param>
        private void ScaleObstaclesSpeed(double multiplyer)
        {
            _obstacleBaseSpeed *= multiplyer;
            _baseObstacleIntervalSeconds *= 1 / multiplyer;
            _obstacleSpeedVariation *= 1 / Math.Sqrt(multiplyer);

            foreach (var obstacle in _obstacles)
            {
                obstacle.Speed = Vector.Multiply(multiplyer, obstacle.Speed);
            }
        }

        /// <summary>
        /// Initialize / reset game
        /// </summary>
        private void InitGame()
        {
            _ball = new Ball(_acc, new Vector(0, 0), new Point(150, ActualHeight / 5 * 2), BallRadius);
            _obstacles = new List<Obstacle>();
            _obstacleBaseSpeed = DefaultObstacleBaseSpeed;
            _baseObstacleIntervalSeconds = DefaultBaseObstacleIntervalSeconds;
            _obstacleSpeedVariation = DefaultObstacleSpeedVariation;

            _musicPlayer.Play();

            _score = 0;

            _dT = TimeSpan.Zero;
            _sumT = TimeSpan.Zero;

            _rightPressed = false;
            _leftPressed = false;
            _upPressed = false;
            _downPressed = false;
            _spacePressed = false;

            _stopWatch.Start();
        }

        /// <summary>
        /// Start game and timers
        /// </summary>
        private void StartGame()
        {
            InitGame();
            _timer.Start();
            _commandRepeatTimer.Start();
        }

        /// <summary>
        /// Process game iteration
        /// </summary>
        /// <param name="elapsed">
        /// The elapsed.
        /// </param>
        private void DoIterateGame(TimeSpan elapsed)
        {
            HandleKeys();
            MoveObjects(elapsed);
            Scoring();
            GenerateObstacles();
        }

        /// <summary>
        /// Generate obstacles if random time elapsed
        /// </summary>
        private void GenerateObstacles()
        {
            double interval = _baseObstacleIntervalSeconds;
            interval += _rand.NextDouble() * Math.Sqrt(_baseObstacleIntervalSeconds);

            if ((DateTime.Now - _prevObstacle).TotalSeconds > interval)
            {
                AddRandomObstacle();
            }
        }

        /// <summary>
        /// Adds a random sized obstacle with random speed
        /// </summary>
        private void AddRandomObstacle()
        {
            double obstacleSpeed = _obstacleBaseSpeed - _rand.NextDouble() * _obstacleSpeedVariation;

            Array values = Enum.GetValues(typeof(ObstacleType));
            ObstacleType randomtype = (ObstacleType)values.GetValue(_rand.Next(values.Length));
            _obstacles.Add(new Obstacle(new Vector(0, 0), new Vector(obstacleSpeed, 0), randomtype, new Rect(0, 0, ActualWidth, ActualHeight)));
            _prevObstacle = DateTime.Now;
        }

        /// <summary>
        /// Call VisualGameObject'g Move method
        /// </summary>
        /// <param name="elapsed">
        /// The elapsed.
        /// </param>
        private void MoveObjects(TimeSpan elapsed)
        {
            if (_ball.Move(elapsed.TotalSeconds, new Rect(0, 0, ActualWidth, ActualHeight)))
            {
                _bouncePlayer.Position = TimeSpan.Zero;
                _bouncePlayer.Play();
            }

            foreach (Obstacle obstacle in _obstacles)
            {
                obstacle.Move(elapsed.TotalSeconds, new Rect(0, 0, ActualWidth, ActualHeight));
            }
        }

        /// <summary>
        /// Calculate score and remove exited obstacles
        /// </summary>
        private void Scoring()
        {
            var obstaclesToRemove = new List<Obstacle>();

            foreach (Obstacle obstacle in _obstacles.Where(obstacle => obstacle.Rectangle.Right < 0))
            {
                obstaclesToRemove.Add(obstacle);
                int scoreValue = (int)Math.Round(obstacle.Difficulty, 0);
                _score += scoreValue;
            }

            foreach (var obstacle in obstaclesToRemove)
            {
                _obstacles.Remove(obstacle);
            }
        }

        /// <summary>
        /// Handle "sticky" command keys
        /// </summary>
        private void HandleKeys()
        {
            if (_upPressed)
            {
                _ball.SuperBounce = true;
                _ball.SmushyBounce = false;
                _upPressed = false;
            }
            else if (_downPressed)
            {
                _ball.SuperBounce = false;
                _ball.SmushyBounce = true;
                _downPressed = false;
            }
        }

        /// <summary>
        /// Stop game processing.
        /// </summary>
        private void GameOver()
        {
            _musicPlayer.Stop();
            _timer.Stop();
            _obstacles.Clear();
            _commandRepeatTimer.Stop();
        }

        #endregion Operation
    }
}
