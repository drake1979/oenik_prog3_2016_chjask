namespace BouncyBall.Helpers
{
    using System;
    using System.IO;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    using Brush = System.Windows.Media.Brush;
    using Point = System.Windows.Point;

    /// <summary>
    /// Helper class to support GameController's rendering
    /// </summary>
    internal static class DrawingTools
    {
        /// <summary>
        /// The transparent piece border.
        /// </summary>
        public static readonly Brush TransparentPieceBorder = new SolidColorBrush(Colors.Transparent);

        /// <summary>
        /// The black piece border.
        /// </summary>
        public static readonly Brush BlackPieceBorder = new SolidColorBrush(Colors.Black);

        /// <summary>
        /// Bitmap source for obstacles
        /// </summary>
        /// <returns>
        /// The <see cref="BitmapSource"/>.
        /// </returns>
        public static BitmapSource ObstacleSource()
        {
            var path = Path.Combine(Environment.CurrentDirectory, "Resources", "obstacleTexture.jpg");

            var uri = new Uri(path);
            BitmapSource bSource = new BitmapImage(uri);

            return bSource;
        }

        /// <summary>
        /// Brush for obstacles
        /// </summary>
        /// <returns>
        /// The <see cref="Brush"/>.
        /// </returns>
        public static Brush ObstacleBrush()
        {
            ImageBrush br = new ImageBrush { ImageSource = ObstacleSource(),  Stretch = Stretch.UniformToFill };
            return br;
        }

        /// <summary>
        /// BitmapSource for ball
        /// </summary>
        /// <returns>
        /// The <see cref="BitmapSource"/>.
        /// </returns>
        public static BitmapSource BirdSource()
        {
            var path = Path.Combine(Environment.CurrentDirectory, "Resources", "birdBall.png");

            var uri = new Uri(path);
            BitmapSource bSource = new BitmapImage(uri);

            return bSource;
        }

        /// <summary>
        /// Brush for ball
        /// </summary>
        /// <returns>
        /// The <see cref="Brush"/>.
        /// </returns>
        public static Brush BallBrush()
        {
            ImageBrush br = new ImageBrush { ImageSource = BirdSource(), Stretch = Stretch.UniformToFill };

            return br;
        }
    }
}