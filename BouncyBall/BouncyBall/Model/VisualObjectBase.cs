﻿namespace BouncyBall.Model
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Abstract base class for all visual objects on the game
    /// </summary>
    public abstract class VisualObjectBase
    {
        /// <summary>
        /// The max speed vector length.
        /// </summary>
        private const double MaxSpeedVectorLength = 50000;

        /// <summary>
        /// The max acceleration vector length.
        /// </summary>
        private const double MaxAccelerationVectorLength = 20000;

        /// <summary>
        /// The speed vector
        /// </summary>
        private Vector _speed;

        /// <summary>
        /// The acceleration vector
        /// </summary>
        private Vector _acceleration;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualObjectBase"/> class.
        /// </summary>
        /// <param name="a0">
        /// Initial acceleration
        /// </param>
        /// <param name="v0">
        /// Initial speed
        /// </param>
        protected VisualObjectBase(Vector a0, Vector v0)
        {
            Acceleration = a0;
            Speed = v0;
        }

        /// <summary>
        /// Gets or sets the speed.
        /// </summary>
        public Vector Speed
        {
            get
            {
                return _speed;
            }

            set
            {
                if (value.Length < MaxSpeedVectorLength)
                {
                    _speed = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the acceleration.
        /// </summary>
        public Vector Acceleration
        {
            get
            {
                return _acceleration;
            }

            set
            {
                if (value.Length < MaxAccelerationVectorLength)
                {
                    _acceleration = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the viual object's rectangle.
        /// </summary>
        public virtual Rect Rectangle { get; set; }

        /// <summary>
        /// Gets or sets the geometry.
        /// </summary>
        public virtual Geometry Geometry { get; set; }

        /// <summary>
        /// The move.
        /// </summary>
        /// <param name="dt">
        /// The time elapsed. Delta t
        /// </param>
        /// <param name="clientrect">
        /// The clientrect.
        /// </param>
        /// <returns>
        /// Returns true if VisualObject specific event occured
        /// </returns>
        public abstract bool Move(double dt, Rect clientrect);

        /// <summary>
        /// Method to determine if the current VisualObject has any intersection with the geometry passed in the parameter.
        /// </summary>
        /// <param name="geometry">
        /// The geometry to check the intersection with
        /// </param>
        /// <returns>
        /// The true if the area of intersection is greater than zero.
        /// </returns>
        public virtual bool IntersectsWith(Geometry geometry)
        {
            if (Rectangle.Right > 0)
            {
                return Geometry.Combine(geometry, Geometry, GeometryCombineMode.Intersect, null).GetArea() > 0;
            }

            return false;
        }

        /// <summary>
        /// Method to determine if the current VisualObject has any intersection with the geometries passed in the parameter.
        /// </summary>
        /// <param name="geometries">
        /// The geometries to check the intersection with
        /// </param>
        /// <returns>
        /// The true if the area of intersection is greater than zero with any of the Geometries passed
        /// </returns>
        public bool IntersectsWith(IEnumerable<Geometry> geometries)
        {
            return geometries.Any(IntersectsWith);
        }
    }
}
