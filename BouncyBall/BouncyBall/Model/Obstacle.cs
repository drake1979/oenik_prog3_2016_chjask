﻿namespace BouncyBall.Model
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class representing the obstacles in the game
    /// </summary>
    public enum ObstacleType
    {
        BottomLow, BottomHigh, Middle, MiddleHigh, TopHigh, TopLow
    }

    /// <summary>
    /// The obstacle.
    /// </summary>
    public class Obstacle : VisualObjectBase
    {
        /// <summary>
        /// The width of the obstacle
        /// </summary>
        private const int Width = 50;

        /// <summary>
        /// The _client rect of the game window. Used only for Difficulty calculation
        /// </summary>
        private Rect _clientRect;

        /// <summary>
        /// Initializes a new instance of the <see cref="Obstacle"/> class.
        /// </summary>
        /// <param name="a0">
        /// INitial acceleration value in px/sec
        /// </param>
        /// <param name="v0">
        /// INitial speed value in px/s
        /// </param>
        /// <param name="type">
        /// Obstacle type
        /// </param>
        /// <param name="clientRect">
        /// Game space
        /// </param>
        public Obstacle(Vector a0, Vector v0, ObstacleType type, Rect clientRect)
            : base(a0, v0)
        {
            double lowHeight = clientRect.Height / 3;
            double highHeight = clientRect.Height / 2;
            _clientRect = clientRect;

            switch (type)
            {
                case ObstacleType.BottomLow:
                    Rectangle = new Rect(
                        new Point(_clientRect.Right, _clientRect.Bottom),
                        new Point(_clientRect.Right + Width, _clientRect.Bottom - lowHeight));
                    break;
                case ObstacleType.BottomHigh:
                    Rectangle = new Rect(
                        new Point(_clientRect.Right, _clientRect.Bottom),
                        new Point(_clientRect.Right + Width, _clientRect.Bottom - highHeight));
                    break;
                case ObstacleType.MiddleHigh:
                    double gap = (_clientRect.Bottom - highHeight) / 2;
                    Rectangle = new Rect(
                        new Point(_clientRect.Right, _clientRect.Bottom - gap),
                        new Point(_clientRect.Right + Width, _clientRect.Bottom - highHeight - gap));
                    break;
                case ObstacleType.Middle:
                    Rectangle = new Rect(
                        new Point(_clientRect.Right, _clientRect.Bottom - lowHeight),
                        new Point(_clientRect.Right + Width, _clientRect.Bottom - lowHeight * 2));
                    break;
                case ObstacleType.TopLow:
                    Rectangle = new Rect(
                        new Point(_clientRect.Right, _clientRect.Top),
                        new Point(_clientRect.Right + Width, _clientRect.Top + lowHeight));
                    break;
                case ObstacleType.TopHigh:
                    Rectangle = new Rect(
                       new Point(_clientRect.Right, _clientRect.Top),
                       new Point(_clientRect.Right + Width, _clientRect.Top + highHeight));
                    break;
            }
        }

        /// <summary>
        /// Calculates the obstacle difficulty from default value, speed, and size
        /// </summary>
        public double Difficulty => 100 + Rectangle.Height / _clientRect.Height * Math.Sqrt(Math.Abs(Speed.X));

        /// <summary>
        /// Gets the obstacle geometry
        /// </summary>
        public override Geometry Geometry => new RectangleGeometry(Rectangle);

        /// <summary>
        /// Moves the obstacle
        /// </summary>
        /// <param name="dt">
        /// Time elapsed
        /// </param>
        /// <param name="clientrect">
        /// The clientrect.
        /// </param>
        /// <returns>
        /// Returns constant false as there is no obstacle specific event handled yet
        /// </returns>
        public override bool Move(double dt, Rect clientrect)
        {
            Vector dr = Vector.Multiply(dt, Speed);
            Rectangle = new Rect(Vector.Add(dr, Rectangle.TopLeft), Vector.Add(dr, Rectangle.BottomRight));

            return false;
        }
    }
}
