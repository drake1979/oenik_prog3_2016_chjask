namespace BouncyBall.Model
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class representing the Ball in the game
    /// </summary>
    public class Ball : VisualObjectBase
    {
        #region Constants

        /// <summary>
        /// Deceleration value for horizontal speed decrease
        /// </summary>
        private const double HorizontalDecelerationValue = 0.1;

        /// <summary>
        /// Deceleration ratio for horizontal speed decrease
        /// </summary>
        private const double HorizontalDecelerationMultiplyer = 0.98;

        /// <summary>
        /// The default bounce coefficient.
        /// </summary>
        private const double DefaultBounceCoefficient = -0.95;

        /// <summary>
        /// Additional speed when SouperBounce
        /// </summary>
        private const double BounceBoost = 40;

        #endregion

        #region Private fields

        /// <summary>
        /// SuperBounce speed multiplyer
        /// </summary>
        private readonly double _superBounceModifier = Math.Sqrt(Math.Sqrt(2));

        #endregion
        
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Ball"/> class.
        /// </summary>
        /// <param name="a0">
        /// The initial acceleration of the ball
        /// </param>
        /// <param name="v0">
        /// The initial speed of the ball
        /// </param>
        /// <param name="position">
        /// The initial position of the ball
        /// </param>
        /// <param name="radius">
        /// The radius of the ball
        /// </param>
        public Ball(Vector a0, Vector v0, Point position, double radius)
            : base(a0, v0)
        {
            Position = position;
            Radius = radius;
            BouncingCoefficient = DefaultBounceCoefficient;
        }

        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the current bounce coefficient
        /// </summary>
        public double BouncingCoefficient { get; set; }

        /// <summary>
        /// Gets or sets the position of the ball
        /// </summary>
        public Point Position { get; set; }

        /// <summary>
        /// Gets or sets the radius of the ball
        /// </summary>
        public double Radius { get; set; }

        /// <summary>
        /// Overrides base Geometry with the Ball's EllipseGeometry
        /// </summary>
        public override Geometry Geometry => new EllipseGeometry(Rectangle);

        /// <summary>
        /// Overrides base Rectangle with the Ball's enclosing Rectangle
        /// </summary>
        public override Rect Rectangle => new Rect(new Point(Position.X - Radius, Position.Y - Radius), new Point(Position.X + Radius, Position.Y + Radius));

        /// <summary>
        /// Gets or sets a value indicating whether to perform super bounce.
        /// </summary>
        public bool SuperBounce { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to perform smushy bounce.
        /// </summary>
        public bool SmushyBounce { get; set; }

        #endregion Properties

        #region Operation

        /// <summary>
        /// Move the ball
        /// </summary>
        /// <param name="dt">
        /// The time elapsed. Delta t.
        /// </param>
        /// <param name="clientrect">
        /// Clent rectangle in which to move or bounce.
        /// </param>
        /// <returns>
        /// Returns true if Ball has been bounced
        /// </returns>
        public override bool Move(double dt, Rect clientrect)
        {
            Vector dv = Vector.Multiply(dt, Acceleration);
            Vector newSpeed = Vector.Add(Speed, dv);
            Vector dr = Vector.Multiply(dt, newSpeed);
            bool result = false;

            // avoid unreal acceleration by lags
            if (dr.Y < clientrect.Height)
            {
                Speed = newSpeed;
            }
            else
            {
                dr = Vector.Multiply(Math.Sqrt(dt), Speed);
            }

            double bounceCoefficient = DefaultBounceCoefficient;

            Point tmp = Point.Add(Position, dr);

            if ((tmp.Y + Radius) > clientrect.Bottom)
            {
                double bottomOverflow = tmp.Y + Radius - clientrect.Bottom;
                dr.Y -= 2 * bottomOverflow;

                double speedOffset = 0;
                if (SuperBounce)
                {
                    bounceCoefficient *= _superBounceModifier;
                    speedOffset = -1 * BounceBoost;
                    SuperBounce = false;
                }

                if (SmushyBounce)
                {
                    bounceCoefficient *= 1 / _superBounceModifier;
                    SmushyBounce = false;
                }

                Speed = new Vector(Speed.X, (Speed.Y * bounceCoefficient) + speedOffset);

                result = true;
            }

            if ((tmp.Y - Radius) < clientrect.Top)
            {
                double topOverflow = tmp.Y - Radius - clientrect.Top;
                dr.Y -= 2 * topOverflow;
                Speed = new Vector(Speed.X, Speed.Y * bounceCoefficient);
                result = true;
            }

            if ((tmp.X + Radius) > clientrect.Right)
            {
                double rightOverflow = tmp.X + Radius - clientrect.Right;
                dr.X -= 2 * rightOverflow;
                Speed = new Vector(Speed.X * bounceCoefficient, Speed.Y);
                result = true;
            }

            if ((tmp.X - Radius) < clientrect.Left)
            {
                double leftOverflow = tmp.X - Radius - clientrect.Left;
                dr.X += 2 * leftOverflow;
                Speed = new Vector(Speed.X * bounceCoefficient, Speed.Y);
                result = true;
            }

            Position = Point.Add(Position, dr);

            DoNotEscape(clientrect);
            HorizontalDeceleration();

            return result;
        }

        /// <summary>
        /// Does not let the ball escape grom the client rectangle when for some reason (lags, debugging etc) the elapsed time would move it outside from it.
        /// </summary>
        /// <param name="clientRect">
        /// The client rect.
        /// </param>
        private void DoNotEscape(Rect clientRect)
        {
            if (Rectangle.Right > clientRect.Right)
            {
                Position = new Point(clientRect.Right - Radius, Position.Y);
            }

            if (Rectangle.Top < clientRect.Top)
            {
                Position = new Point(Position.X, Radius);
            }

            if (Rectangle.Bottom > clientRect.Bottom)
            {
                Position = new Point(Position.X, clientRect.Bottom - Radius);
            }

            if (Rectangle.Left < clientRect.Left)
            { 
                Position = new Point(clientRect.Left + Radius, clientRect.Top + Position.Y);
            }
        }

        /// <summary>
        /// Decelerates the ball's horizontal speed like some kind of drag
        /// </summary>
        private void HorizontalDeceleration()
        {
            double vX = Speed.X;
            if (Math.Abs(vX) > 0)
            {
                double sign = vX / Math.Abs(vX);
                vX = Math.Abs(vX);
                vX -= HorizontalDecelerationValue;
                vX *= HorizontalDecelerationMultiplyer;
                Speed = new Vector(vX * sign, Speed.Y);
            }
        }

        #endregion
    }
}